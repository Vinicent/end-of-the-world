﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndOfTheWorld
{
    public class SeifeMunition : MonoBehaviour
    {
        //Setzt die Geschwindigkeit und den Dmg der Seife fest
        public float speed;
        private int damage = 1;
        public Rigidbody2D rigidBody;


        void Start()
        {
            //hierdurch bewegt sich die Seife nach rechts
            rigidBody.velocity = transform.right * speed;
        }

        void OnTriggerEnter2D(Collider2D hitInfo)
        {
            //wenn die Seife einen Gegner trifft, fügt sie ihm schaden zu
            Enemy enemy = hitInfo.GetComponent<Enemy>();
            if (enemy == null)
            {
                enemy = hitInfo.GetComponentInParent<Enemy>();
            }

            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }
            //Egal was die Seife berührt sie wird danach zerstört
            Destroy(gameObject);
        }

    }
}