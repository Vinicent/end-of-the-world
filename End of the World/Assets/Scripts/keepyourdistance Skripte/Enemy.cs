﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace EndOfTheWorld
{
    public class Enemy : MonoBehaviour
    {
        public int health = 1;
        [SerializeField]
        private float speed;
        [SerializeField]
        private bool goRight;
        [SerializeField]
        private bool patrolEnabled;
        [SerializeField]
        private float patrolDistance;
        private Vector3 initialPosition;
        private int direction = 1;

        public GameObject destroyObject;
        private GameObject player;

        private void Awake()
        {
            player = GameObject.Find("Player");
            initialPosition = transform.position;
        }

        void Update()
        {
            if (patrolEnabled)
            {
                if (goRight)
                {
                    if (transform.position.x >= initialPosition.x + patrolDistance)
                    {
                        direction = -1;
                    }
                    else if (transform.position.x <= initialPosition.x)
                    {
                        direction = 1;
                    }
                    transform.position += new Vector3(speed * Time.deltaTime * direction, 0, 0);
                }
                else
                {
                    if (transform.position.x <= initialPosition.x - patrolDistance)
                    {
                        direction = 1;
                    }
                    else if (transform.position.x >= initialPosition.x)
                    {
                        direction = -1;
                    }
                    transform.position += new Vector3(speed * Time.deltaTime * direction, 0, 0);
                }
            }
            else
            {
                if (goRight == false)
                {
                    transform.position += Vector3.left * speed * Time.deltaTime;
                }
                else
                {
                    transform.position += Vector3.right * speed * Time.deltaTime;
                }
            }

        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                GameEvents.current.Damaged();
            }

        }
        public void TakeDamage(int damage)
        {
            health -= damage;

            if (health <= 0)
            {
                Die();
            }
        }

        void Die()
        {
            Collider2D collider = GetComponent<Collider2D>();

            if (collider == null)
            {
                collider = GetComponentInChildren<Collider2D>();
            }

            collider.enabled = false;
            destroyObject.SetActive(false);
        }
    }

}
