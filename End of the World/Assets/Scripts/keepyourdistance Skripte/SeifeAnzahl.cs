﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace EndOfTheWorld
{
    public class SeifeAnzahl : MonoBehaviour
    {
        public static SeifeAnzahl instance;
        public TextMeshProUGUI text;
        void Start()
        {//es soll nur eine instanz von seifeAnzahl pro spiel geben
            if (instance == null)
            {
                instance = this;
            }
        }
        //zeigt die Munitionsanzahl im UI an
        public void ChangeScore(int munition)
        {
            text.text = "X" + munition.ToString();
        }
    }
}