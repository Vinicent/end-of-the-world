﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EndOfTheWorld
{
 //holds 3 "heart" sprites , the sprite for the game over scenario and a soundeffect
    public class Health : MonoBehaviour
    {
        public GameObject heart1, heart2, heart3, gameOver;
        public static int health;
        //public AudioSource cough;
        private bool isGameOver = false;

        private void Start()
        {
            GameEvents.current.DamageReceived += healthDown;
            health = 3;
            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(true);
            heart3.gameObject.SetActive(true);
            gameOver.gameObject.SetActive(false);
        }

        private void healthUp()
        {
            health = 1;
        }
        
        private void healthDown()
        {
            health -= 1;
        }
            //switch cases for different scenarios- if 1 dmg is taken, disable 1 heart, and so on
        //0 hearts > Game over screen gets enabled + sound
        private void Update()
        {
            if (health > 3)
                health = 3;

            switch(health)
            {
                case 3:
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(true);
                    heart3.gameObject.SetActive(true);
                    break;
                case 2:
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(true);
                    heart3.gameObject.SetActive(false);
                    break;
                case 1:
                    heart1.gameObject.SetActive(true);
                    heart2.gameObject.SetActive(false);
                    heart3.gameObject.SetActive(false);
                    break;
                case 0:
                    heart1.gameObject.SetActive(false);
                    heart2.gameObject.SetActive(false);
                    heart3.gameObject.SetActive(false);
                    gameOver.gameObject.SetActive(true);

                    if (!isGameOver)
                    {
                        //cough.Play();
                        isGameOver = true;
                    }
 //During Game over, we lock all input by setting the game time to 0.
                    Time.timeScale = 0;

                    if(Input.anyKeyDown)
                    {//change scene by pressing any key
                        SceneManager.LoadScene("1_Menu");
                        Time.timeScale = 1;
                    }

                    break;

            }
        }
    }
}
