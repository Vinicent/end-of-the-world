﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndOfTheWorld
{
    public class CameraFollow : MonoBehaviour
    {
        public Transform playerTransform;
        public float smoothSpeed = 0.0f;
        public Vector3 offset;

        void LateUpdate()
        {
            // hier speichern wir die aktuelle Kameraposition
            Vector3 tempPlayerPosition = playerTransform.position;
            Vector3 tempCameraPosition = new Vector3(tempPlayerPosition.x + offset.x, transform.position.y, transform.position.z);
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, tempCameraPosition, smoothSpeed);
            transform.position = smoothedPosition;
        }
    }

}
