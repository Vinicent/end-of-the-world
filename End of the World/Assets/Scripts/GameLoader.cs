using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EndOfTheWorld
{
    public class GameLoader : MonoBehaviour
    {
        public string sceneName;
        public void Reset()
        {
            SceneManager.LoadScene(sceneName);
        }
        public void Quit()
        {
            Application.Quit();
            Debug.Log("Game Quittet");
        }
    }
}
