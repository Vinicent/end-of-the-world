﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using EndOfTheWorld;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    void Awake()
    {
        current = this;
    }

    public event Action DamageReceived;
    public void Damaged()
    {
        if (DamageReceived != null)
        {
            DamageReceived();
        }
    }
}
