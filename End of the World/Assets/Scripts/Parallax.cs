﻿using UnityEngine;

namespace EndOfTheWorld
{ 
    public class Parallax : MonoBehaviour
    {
        private float length;
        
        private float startPos;
        //camera position
        public GameObject cam;
       // the "intensitiy" of the parralax effect. 1=same speed as camera
        public float parallaxEffect = 1f;
        
        void Start()
        { 
            //remember camera starting position
            startPos = transform.position.x;
            length = GetComponent<SpriteRenderer>().bounds.size.x;
        }
    
        void FixedUpdate()
        {
            float temp = (cam.transform.position.x * (1 - parallaxEffect));
            //calculate the new postion for the GO based on camera postion and parallax intensity
            float dist = (cam.transform.position.x * parallaxEffect);
            //set new position
            transform.position = new Vector3(startPos + dist, transform.position.y, transform.position.z);

            if (temp > startPos + length)
            {
                startPos += length;
            }
            
            else if(temp < startPos - length)
            {
                startPos -= length;
            }
        }
    }

}
