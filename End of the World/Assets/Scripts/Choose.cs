using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EndOfTheWorld
{
    public class Choose : MonoBehaviour
    {
        public void Girl()
        {
            SceneManager.LoadScene("End01");
        }
        
        public void BadGuy()
        {
            SceneManager.LoadScene("End02");
        }
    }
}
