using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EndOfTheWorld
{
public class Player : MonoBehaviour
{
    public float runSpeed;
    [SerializeField] 
    private float m_JumpForce = 400f;
    public bool jump = false;
    public bool canMove = true;

    [SerializeField] 
    private Transform m_GroundCheck;
    [SerializeField] 
    private LayerMask m_WhatIsGround;
    
    public bool m_Grounded;
    const float k_GroundedRadius = .2f;
    private Rigidbody2D m_Rigidbody2D;

    Vector3 originalPos;
    //public AudioSource maeh;
    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

    }

    private void Start()
    {
        originalPos = transform.position;
            InvokeRepeating("SaveTransform", 0.0f, 5.0f);
    }
    void Update()
    {
        this.transform.position = this.transform.position + (new Vector3(runSpeed, 0, 0) * Time.deltaTime);


        if (Input.GetButtonDown("Jump"))
        {
            //maeh.Play();
            jump = true;

        }
    }
    void FixedUpdate()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
            }
        }

        if (!canMove)
        {
            return;
        }
        // Move our character
        Jump(runSpeed * Time.fixedDeltaTime, jump);
        jump = false;
    }

        public void SaveTransform()
        {
            if(m_Grounded == true)
            {
            originalPos = transform.position;
            }
        }
    public void Jump(float runSpeed, bool jump)
    {
        // If the player should jump...
        if (m_Grounded && jump)
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
        }
    }
        
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Fail"))
        {            
            transform.position = originalPos;
                GameEvents.current.Damaged();
        }
        if (other.CompareTag("Goal"))
        {
                SceneManager.LoadScene("4_Cutscene");
        }
    
    }  
}
}