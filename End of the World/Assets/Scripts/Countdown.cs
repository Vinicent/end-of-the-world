using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndOfTheWorld
{
    public class Countdown : MonoBehaviour
    {
        public AudioSource bang;
        public void Ending()
        {
            if (Input.anyKey)
            {
                bang.Play();
                if (Input.anyKey)
                {
                    Application.Quit();
                    Debug.Log("Game Over");
                }
            }
        }
    }
}
